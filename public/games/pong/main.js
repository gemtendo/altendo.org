/*
canvas = document.getElementById("canvas");
ctx = canvas.getContext("2d");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

ctx.fillStyle = "#00FF00";
ctx.fillRect(0, 0, 200, 200);

console.log(canvas.width);
console.log(canvas.height);

const { width, height } = canvas.getBoundingClientRect();

console.log(canvas.width);
console.log(canvas.height);
*/

pong = document.getElementById("pong");

rightpaddle = document.getElementById("right-paddle");
rightpaddle.addEventListener("mouseover", onPaddle);

leftpaddle = document.getElementById("left-paddle");
leftpaddle.addEventListener("click", onPaddleClick);

function onPaddle() {
	rightpaddle.setAttribute("fill", "red");
}

function onPaddleClick() {
	leftpaddle.setAttribute("fill", "blue");
}

//rightpaddle_pos = [
y = 0;

function pan(event) {
	event.preventDefault();
	y += event.deltaY;

	rightpaddle.setAttribute("y", y);
}

pong.onwheel = pan;


ball = document.getElementById("ball");
cx = 50.0;
cy = 50.0;

direction = [-1.0, 0.0];

function tick() {
	cx -= 0.1;
	ball.setAttribute("cx", cx + '%');
}

setInterval(tick, 16.6);
